# FS4 Footswitch MIDI

Making the Laney FS4 Footswitch ready for use as MIDI Device. The pedal uses a standard MIDI cable but uses a different PIN Layout as the standardizes MIDI Connector.

## How the connector is wired

I found this wiring diagram online, but it turned out to be not exactly the wiring my pedal used.

<img src="Images/wiring_diagram.jpg" alt="wiring online" width="250"/>

It tinkered around and found the following pinout to be working:

<img src="Images/actual_wiring_as_seen_from_backside_of_switch.jpg" alt="actual wiring" width="250"/>

## Wiring diagram

The wiring is really simple as we just need to add some pull-down resistors to every switch wire and connect the wires to the analog pins.
(As it got a bit messy it can be, that i messed up the exact wiring of the switches. Maybe you have to check, wether the correct switch is connected to the right A-Pin.
I can debug this again if this project picks up some interest. For my usecase it works this way)

TODO: Add wiring diagram

(I realized a small error I did there: In the setup with the amp, the Amp delivers the needed voltages for the specific LEDs via the outer pins of the connector. In my configuration this is done the opposite way. This creates a small inconvenience: The voltage at the analogue pins never goes to GND. But the code still works the way intended. If I find the time, I will design this the other way around and upload it here.) 

## Code & Arduino

I used an Arduino UNO, but this project can be done with any of the arduino microcontrollers. As the chip on the Arduino UNO is not able to use the USBMIDI Library, we need some additional software to read the MIDI data from serial.

## Additional Software

To read the data from serial, I used the "Hairless MIDI to Serial Bridge" Software: [][HAIRLESS-MIDI](https://projectgus.github.io/hairless-midiserial/)
To then use the switch as a controller in any DAW, we need to set up "loopMIDI": [][loopMIDI](https://www.tobias-erichsen.de/software/loopmidi.html)

<img src="Images/loopMidi.png" alt="loopMIDI" width="250"/>
