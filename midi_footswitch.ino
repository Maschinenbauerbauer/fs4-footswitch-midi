#include <AnalogEvent.h>

int red_left = A0;
int green = A1;
int yellow = A3;
int red_right = A2;

int last_red_left_status = 0;
int last_green_status = 0;
int last_yellow_status = 0;
int last_red_right_status = 0;

void setup() {
  AnalogEvent.addAnalogPort(
    red_left,        //potentiometer pin
    onChange, //onChange event function
    200);       //hysteresis
  AnalogEvent.addAnalogPort(
    green,        //potentiometer pin
    onChange, //onChange event function
    200);       //hysteresis
  AnalogEvent.addAnalogPort(
    yellow,        //potentiometer pin
    onChange, //onChange event function
    200);       //hysteresis
  AnalogEvent.addAnalogPort(
    red_right,//potentiometer pin
    onChange, //onChange event function
    200);       //hysteresis
  Serial.begin(115200);
}

void loop() {
  AnalogEvent.loop();
}

void onChange(AnalogPortInformation* Sender) {
  //Serial.println(Sender->pin);
  //Serial.println(byte(Sender->value));

  switch (Sender->pin) {
    case A0:
      if (Sender->value > 100) {
        sendCmd(0x90, 0x7F, 0x45);
      } else {sendCmd(0x80, 0x7F, 0x00);}
      break;
    case A1:
      if (Sender->value > 100) {
        sendCmd(0x90, 0x7E, 0x45);
      } else {sendCmd(0x80, 0x7E, 0x00);}
      break;
    case A3:
      if (Sender->value > 100) {
        sendCmd(0x90, 0x7D, 0x45);
      } else {sendCmd(0x80, 0x7D, 0x00);}
      break;
    case A2:
      if (Sender->value > 100) {
        sendCmd(0x90, 0x7C, 0x45);
      } else {sendCmd(0x80, 0x7C, 0x00);}
      break;
  }
  delay(100);
}

void sendCmd(int cmd, int pitch, int velocity) {
  Serial.write(cmd);
  Serial.write(pitch);
  Serial.write(velocity);
}
